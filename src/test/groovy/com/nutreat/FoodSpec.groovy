package com.nutreat

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class FoodSpec extends Specification implements DomainUnitTest<Food> {

    def setup() {
        new MeasureUnit(unit: 'ounces', remarks: 'measure unit to share in tests').save()
        new FoodGroup(name: 'fruits', remarks: 'food group to share in tests').save()
    }

    void 'food requires existing measure unit and food group'() {
        when:
        domain.name = 'apple'

        then:
        !domain.validate()
        domain.errors['measureUnit'].code == 'nullable'
        domain.errors['foodGroup'].code == 'nullable'

        when:'good group bad units'
        domain.measureUnit = MeasureUnit.findByUnit('non existent unit')
        domain.foodGroup = FoodGroup.findByName('fruits')

        then:
        !domain.validate()
        domain.errors['measureUnit'].code == 'nullable'

        when:'good units bad group'
        domain.measureUnit = MeasureUnit.findByUnit('ounces')
        domain.foodGroup = FoodGroup.findByName('non existent group')

        then:
        !domain.validate()
        domain.errors['foodGroup'].code == 'nullable'

        when:'booth units and group are good'
        domain.measureUnit = MeasureUnit.findByUnit('ounces')
        domain.foodGroup = FoodGroup.findByName('fruits')

        then:
        domain.validate()
    }
}
