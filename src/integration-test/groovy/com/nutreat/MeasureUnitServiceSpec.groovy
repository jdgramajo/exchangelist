package com.nutreat

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class MeasureUnitServiceSpec extends Specification {

    MeasureUnitService measureUnitService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new MeasureUnit(...).save(flush: true, failOnError: true)
        //new MeasureUnit(...).save(flush: true, failOnError: true)
        //MeasureUnit measureUnit = new MeasureUnit(...).save(flush: true, failOnError: true)
        //new MeasureUnit(...).save(flush: true, failOnError: true)
        //new MeasureUnit(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //measureUnit.id
    }

    void "test get"() {
        setupData()

        expect:
        measureUnitService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<MeasureUnit> measureUnitList = measureUnitService.list(max: 2, offset: 2)

        then:
        measureUnitList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        measureUnitService.count() == 5
    }

    void "test delete"() {
        Long measureUnitId = setupData()

        expect:
        measureUnitService.count() == 5

        when:
        measureUnitService.delete(measureUnitId)
        sessionFactory.currentSession.flush()

        then:
        measureUnitService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        MeasureUnit measureUnit = new MeasureUnit()
        measureUnitService.save(measureUnit)

        then:
        measureUnit.id != null
    }
}
