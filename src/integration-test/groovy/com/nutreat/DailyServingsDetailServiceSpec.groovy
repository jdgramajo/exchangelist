package com.nutreat

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class DailyServingsDetailServiceSpec extends Specification {

    DailyServingsDetailService dailyServingsDetailService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new DailyServingsDetail(...).save(flush: true, failOnError: true)
        //new DailyServingsDetail(...).save(flush: true, failOnError: true)
        //DailyServingsDetail dailyServingsDetail = new DailyServingsDetail(...).save(flush: true, failOnError: true)
        //new DailyServingsDetail(...).save(flush: true, failOnError: true)
        //new DailyServingsDetail(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //dailyServingsDetail.id
    }

    void "test get"() {
        setupData()

        expect:
        dailyServingsDetailService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<DailyServingsDetail> dailyServingsDetailList = dailyServingsDetailService.list(max: 2, offset: 2)

        then:
        dailyServingsDetailList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        dailyServingsDetailService.count() == 5
    }

    void "test delete"() {
        Long dailyServingsDetailId = setupData()

        expect:
        dailyServingsDetailService.count() == 5

        when:
        dailyServingsDetailService.delete(dailyServingsDetailId)
        sessionFactory.currentSession.flush()

        then:
        dailyServingsDetailService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        DailyServingsDetail dailyServingsDetail = new DailyServingsDetail()
        dailyServingsDetailService.save(dailyServingsDetail)

        then:
        dailyServingsDetail.id != null
    }
}
