package com.nutreat

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class FoodGroupServiceSpec extends Specification {

    FoodGroupService foodGroupService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new FoodGroup(...).save(flush: true, failOnError: true)
        //new FoodGroup(...).save(flush: true, failOnError: true)
        //FoodGroup foodGroup = new FoodGroup(...).save(flush: true, failOnError: true)
        //new FoodGroup(...).save(flush: true, failOnError: true)
        //new FoodGroup(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //foodGroup.id
    }

    void "test get"() {
        setupData()

        expect:
        foodGroupService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<FoodGroup> foodGroupList = foodGroupService.list(max: 2, offset: 2)

        then:
        foodGroupList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        foodGroupService.count() == 5
    }

    void "test delete"() {
        Long foodGroupId = setupData()

        expect:
        foodGroupService.count() == 5

        when:
        foodGroupService.delete(foodGroupId)
        sessionFactory.currentSession.flush()

        then:
        foodGroupService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        FoodGroup foodGroup = new FoodGroup()
        foodGroupService.save(foodGroup)

        then:
        foodGroup.id != null
    }
}
