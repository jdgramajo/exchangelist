package com.nutreat

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class DailyServingsHeaderServiceSpec extends Specification {

    DailyServingsHeaderService dailyServingsHeaderService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new DailyServingsHeader(...).save(flush: true, failOnError: true)
        //new DailyServingsHeader(...).save(flush: true, failOnError: true)
        //DailyServingsHeader dailyServingsHeader = new DailyServingsHeader(...).save(flush: true, failOnError: true)
        //new DailyServingsHeader(...).save(flush: true, failOnError: true)
        //new DailyServingsHeader(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //dailyServingsHeader.id
    }

    void "test get"() {
        setupData()

        expect:
        dailyServingsHeaderService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<DailyServingsHeader> dailyServingsHeaderList = dailyServingsHeaderService.list(max: 2, offset: 2)

        then:
        dailyServingsHeaderList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        dailyServingsHeaderService.count() == 5
    }

    void "test delete"() {
        Long dailyServingsHeaderId = setupData()

        expect:
        dailyServingsHeaderService.count() == 5

        when:
        dailyServingsHeaderService.delete(dailyServingsHeaderId)
        sessionFactory.currentSession.flush()

        then:
        dailyServingsHeaderService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        DailyServingsHeader dailyServingsHeader = new DailyServingsHeader()
        dailyServingsHeaderService.save(dailyServingsHeader)

        then:
        dailyServingsHeader.id != null
    }
}
