package com.nutreat

class DailyServingsDetail implements Comparable {
    FoodGroup foodGroup
    Double amount
    String remarks

    static belongsTo = [dailyServingsHeader: DailyServingsHeader]

    int compareTo(obj) {
        foodGroup.compareTo(obj.foodGroup)
    }

    static constraints = {
        foodGroup blank: false
        amount blank: false
        remarks nullable: true, blank: true
        dailyServingsHeader nullable: false
    }
}
