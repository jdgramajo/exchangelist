package com.nutreat

class DailyServingsHeader {
    Integer kcal
    String remarks

    static hasMany = [dailyServingsDetail: DailyServingsDetail]

    static constraints = {
        kcal blank: false, unique: false
        remarks nullable: true, blank: true
    }
}
