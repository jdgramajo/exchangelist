package com.nutreat

class Food {
    String name
    MeasureUnit measureUnit
    String remarks

    static belongsTo = [foodGroup: FoodGroup]

    static constraints = {
        name blank: false, unique: true
        measureUnit blank: false
        remarks nullable: true, blank: true
        foodGroup nullable: false
    }
}
