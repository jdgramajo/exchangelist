package com.nutreat

class FoodGroup implements Comparable {
    String name
    String remarks

    static hasMany = [food: Food]

    int compareTo(obj) {
        name.compareTo(obj.name)
    }

    static constraints = {
        name blank: false, unique: true
        remarks nullable: true, blank: true
    }
}
