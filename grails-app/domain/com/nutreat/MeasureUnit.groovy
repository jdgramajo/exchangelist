package com.nutreat

class MeasureUnit {
    String unit
    String remarks

    static constraints = {
        unit blank: false, unique: true
        remarks nullable: true, blank: true
    }
}
