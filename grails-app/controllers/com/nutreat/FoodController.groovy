package com.nutreat

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class FoodController {

    FoodService foodService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond foodService.list(params), model:[foodCount: foodService.count()]
    }

    def show(Long id) {
        respond foodService.get(id)
    }

    def create() {
        respond new Food(params)
    }

    def save(Food food) {
        if (food == null) {
            notFound()
            return
        }

        try {
            foodService.save(food)
        } catch (ValidationException e) {
            respond food.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'food.label', default: 'Food'), food.id])
                redirect food
            }
            '*' { respond food, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond foodService.get(id)
    }

    def update(Food food) {
        if (food == null) {
            notFound()
            return
        }

        try {
            foodService.save(food)
        } catch (ValidationException e) {
            respond food.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'food.label', default: 'Food'), food.id])
                redirect food
            }
            '*'{ respond food, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        foodService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'food.label', default: 'Food'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'food.label', default: 'Food'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
