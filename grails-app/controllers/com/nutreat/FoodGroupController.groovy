package com.nutreat

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class FoodGroupController {

    FoodGroupService foodGroupService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond foodGroupService.list(params), model:[foodGroupCount: foodGroupService.count()]
    }

    def show(Long id) {
        respond foodGroupService.get(id)
    }

    def create() {
        respond new FoodGroup(params)
    }

    def save(FoodGroup foodGroup) {
        if (foodGroup == null) {
            notFound()
            return
        }

        try {
            foodGroupService.save(foodGroup)
        } catch (ValidationException e) {
            respond foodGroup.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'foodGroup.label', default: 'FoodGroup'), foodGroup.id])
                redirect foodGroup
            }
            '*' { respond foodGroup, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond foodGroupService.get(id)
    }

    def update(FoodGroup foodGroup) {
        if (foodGroup == null) {
            notFound()
            return
        }

        try {
            foodGroupService.save(foodGroup)
        } catch (ValidationException e) {
            respond foodGroup.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'foodGroup.label', default: 'FoodGroup'), foodGroup.id])
                redirect foodGroup
            }
            '*'{ respond foodGroup, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        foodGroupService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'foodGroup.label', default: 'FoodGroup'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'foodGroup.label', default: 'FoodGroup'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
