package com.nutreat

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class DailyServingsDetailController {

    DailyServingsDetailService dailyServingsDetailService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond dailyServingsDetailService.list(params), model:[dailyServingsDetailCount: dailyServingsDetailService.count()]
    }

    def show(Long id) {
        respond dailyServingsDetailService.get(id)
    }

    def create() {
        respond new DailyServingsDetail(params)
    }

    def save(DailyServingsDetail dailyServingsDetail) {
        if (dailyServingsDetail == null) {
            notFound()
            return
        }

        try {
            dailyServingsDetailService.save(dailyServingsDetail)
        } catch (ValidationException e) {
            respond dailyServingsDetail.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'dailyServingsDetail.label', default: 'DailyServingsDetail'), dailyServingsDetail.id])
                redirect dailyServingsDetail
            }
            '*' { respond dailyServingsDetail, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond dailyServingsDetailService.get(id)
    }

    def update(DailyServingsDetail dailyServingsDetail) {
        if (dailyServingsDetail == null) {
            notFound()
            return
        }

        try {
            dailyServingsDetailService.save(dailyServingsDetail)
        } catch (ValidationException e) {
            respond dailyServingsDetail.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'dailyServingsDetail.label', default: 'DailyServingsDetail'), dailyServingsDetail.id])
                redirect dailyServingsDetail
            }
            '*'{ respond dailyServingsDetail, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        dailyServingsDetailService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'dailyServingsDetail.label', default: 'DailyServingsDetail'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dailyServingsDetail.label', default: 'DailyServingsDetail'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
