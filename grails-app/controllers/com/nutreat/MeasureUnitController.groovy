package com.nutreat

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class MeasureUnitController {

    MeasureUnitService measureUnitService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond measureUnitService.list(params), model:[measureUnitCount: measureUnitService.count()]
    }

    def show(Long id) {
        respond measureUnitService.get(id)
    }

    def create() {
        respond new MeasureUnit(params)
    }

    def save(MeasureUnit measureUnit) {
        if (measureUnit == null) {
            notFound()
            return
        }

        try {
            measureUnitService.save(measureUnit)
        } catch (ValidationException e) {
            respond measureUnit.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'measureUnit.label', default: 'MeasureUnit'), measureUnit.id])
                redirect measureUnit
            }
            '*' { respond measureUnit, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond measureUnitService.get(id)
    }

    def update(MeasureUnit measureUnit) {
        if (measureUnit == null) {
            notFound()
            return
        }

        try {
            measureUnitService.save(measureUnit)
        } catch (ValidationException e) {
            respond measureUnit.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'measureUnit.label', default: 'MeasureUnit'), measureUnit.id])
                redirect measureUnit
            }
            '*'{ respond measureUnit, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        measureUnitService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'measureUnit.label', default: 'MeasureUnit'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'measureUnit.label', default: 'MeasureUnit'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
