package com.nutreat

import grails.validation.ValidationException

import javax.swing.SpringLayout.Constraints

import static org.springframework.http.HttpStatus.*

class DailyServingsHeaderController {

    DailyServingsHeaderService dailyServingsHeaderService
    DailyServingsDetailService dailyServingsDetailService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond dailyServingsHeaderService.list(params), model:[dailyServingsHeaderCount: dailyServingsHeaderService.count()]
    }

    def show(Long id) {
        respond dailyServingsHeaderService.get(id)
    }

    def create() {
        DailyServingsInfo dailyServingsInfo = new DailyServingsInfo()
        List<FoodGroup> foodGroupList = FoodGroup.findAll(sort: "name")
        foodGroupList.each {
            dailyServingsInfo.foodGroupInfo.put(it.name, 0.0)
        }
        render(view: "create", model: [dailyServingsInfo: dailyServingsInfo])
    }

    def save(DailyServingsInfo dailyServingsInfo) {
        
        DailyServingsHeader dailyServingsHeader = new DailyServingsHeader(kcal: dailyServingsInfo.kcal,
            remarks: dailyServingsInfo.remarks)

        params.keySet().findAll { it.contains '_servings' }.each {
            FoodGroup foodGroup = FoodGroup.findByName(it.minus('_servings'))
            Double amount = params[it].toDouble()
            if ( amount > 0.0 ) {
                dailyServingsHeader.addToDailyServingsDetail(foodGroup: foodGroup, amount: amount)
            }
        }

        try {
            dailyServingsHeaderService.save(dailyServingsHeader)
        } catch (ValidationException e) {
            respond dailyServingsHeader.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', 
                    args: [message(code: 'dailyServingsHeader.label', 
                    default: 'DailyServingsHeader'), dailyServingsHeader.id])
                redirect dailyServingsHeader
            }
            '*' { respond dailyServingsHeader, [status: CREATED] }
        }
    }

    def edit(Long id) {
        List<FoodGroup> foodGroupList = FoodGroup.findAll(sort: "name")
        DailyServingsInfo dailyServingsInfo = new DailyServingsInfo()
        DailyServingsHeader dailyServingsHeader = dailyServingsHeaderService.get(id)
        dailyServingsInfo.id = dailyServingsHeader.id
        dailyServingsInfo.version = dailyServingsHeader.version
        dailyServingsInfo.kcal = dailyServingsHeader.kcal
        dailyServingsInfo.remarks = dailyServingsHeader.remarks
        foodGroupList.each {
            dailyServingsInfo.foodGroupInfo.put(it.name, 0.0)
        }
        dailyServingsHeader.dailyServingsDetail.each {
            dailyServingsInfo.foodGroupInfo.put(it.foodGroup.name, it.amount)
        }
        render(view: "edit", model: [dailyServingsInfo: dailyServingsInfo])
    }

    def update(DailyServingsInfo dailyServingsInfo) {

        if (dailyServingsInfo.hasErrors()) {
            respond dailyServingsInfo.errors, view 'edit'
            return
        }
        
        DailyServingsHeader dailyServingsHeader = dailyServingsHeaderService.get(dailyServingsInfo.id)
        
        if (dailyServingsHeader == null) {
            notFound()
            return
        }
        //TODO:check version
        dailyServingsHeader.kcal = dailyServingsInfo.kcal.toInteger()
        dailyServingsHeader.remarks = dailyServingsInfo.remarks

        params.keySet().findAll { it.contains '_servings' }.each {
            FoodGroup foodGroup = FoodGroup.findByName(it.minus('_servings'))
            DailyServingsDetail dailyServingsDetail = dailyServingsHeaderService
                .findDailyServingsDetailByFoodGroup(dailyServingsHeader, foodGroup)
            Double amount = params[it].toDouble()
            if ( amount > 0.0 ) {
                if ( dailyServingsDetail ) {
                    dailyServingsDetail.amount = amount
                    dailyServingsDetail.save()
                } else {
                    dailyServingsHeader.addToDailyServingsDetail(foodGroup: foodGroup, amount: amount)
                }
            } else if ( dailyServingsDetail ) {
                dailyServingsDetailService.delete(dailyServingsDetail.id)
            }
        }

        try {
            dailyServingsHeaderService.save(dailyServingsHeader)
        } catch (ValidationException e) {
            respond dailyServingsHeader.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'dailyServingsHeader.label', default: 'DailyServingsHeader'), dailyServingsHeader.id])
                redirect dailyServingsHeader
            }
            '*'{ respond dailyServingsHeader, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        dailyServingsHeaderService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'dailyServingsHeader.label', default: 'DailyServingsHeader'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'dailyServingsHeader.label', default: 'DailyServingsHeader'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

class DailyServingsInfo {
    Long id
    Long version
    String kcal
    String remarks
    Map<String, Double> foodGroupInfo = new TreeMap<>()

    static constraints = {
        kcal blank: false, unique: false
        remarks blank: true, nullable: true
    }
}
