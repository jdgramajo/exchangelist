package com.nutreat

import grails.gorm.services.Service

@Service(DailyServingsHeader)
interface DailyServingsHeaderService {

    DailyServingsHeader get(Serializable id)

    List<DailyServingsHeader> list(Map args)

    Long count()

    void delete(Serializable id)

    DailyServingsHeader save(DailyServingsHeader dailyServingsHeader)

    DailyServingsDetail findDailyServingsDetailByFoodGroup(DailyServingsHeader dailyServingsHeader, 
        FoodGroup foodGroup)

}