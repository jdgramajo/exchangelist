package com.nutreat

import grails.gorm.services.Service

@Service(Food)
interface FoodService {

    Food get(Serializable id)

    List<Food> list(Map args)

    Long count()

    void delete(Serializable id)

    Food save(Food food)

}