package com.nutreat

import grails.gorm.services.Service

@Service(DailyServingsDetail)
interface DailyServingsDetailService {

    DailyServingsDetail get(Serializable id)

    List<DailyServingsDetail> list(Map args)

    Long count()

    void delete(Serializable id)

    DailyServingsDetail save(DailyServingsDetail dailyServingsDetail)

}