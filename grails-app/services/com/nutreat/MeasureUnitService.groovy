package com.nutreat

import grails.gorm.services.Service

@Service(MeasureUnit)
interface MeasureUnitService {

    MeasureUnit get(Serializable id)

    List<MeasureUnit> list(Map args)

    Long count()

    void delete(Serializable id)

    MeasureUnit save(MeasureUnit measureUnit)

}