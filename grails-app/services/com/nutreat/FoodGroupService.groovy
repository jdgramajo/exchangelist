package com.nutreat

import grails.gorm.services.Service

@Service(FoodGroup)
interface FoodGroupService {

    FoodGroup get(Serializable id)

    List<FoodGroup> list(Map args)

    Long count()

    void delete(Serializable id)

    FoodGroup save(FoodGroup foodGroup)

}