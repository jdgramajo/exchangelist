<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'food.label', default: 'Food')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#show-food" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-food" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <!--f:display bean="food" /-->
            <ol class="property-list">
                <li class="fieldcontain">
                    <span id="name-label" class="property-label">
                        <g:message code="food.name.label" default="Alimento" />
                    </span>
                    <div class="property-value"><f:display bean="food" property="name" /></div>
                </li>
                <li class="fieldcontain">
                    <span id="name-label" class="property-label">
                        <g:message code="food.measureUnit.label" default="Unidad de medida" />
                    </span>
                    <f:display bean="food" property="measureUnit" />
                </li>
                <li class="fieldcontain">
                    <span id="name-label" class="property-label">
                        <g:message code="food.remarks.label" default="Observaciones" />
                    </span>
                    <div class="property-value"><f:display bean="food" property="remarks" /></div>
                </li>
                <li class="fieldcontain">
                    <span id="name-label" class="property-label">
                        <g:message code="food.foodGroup.label" default="Grupo Alimenticio" />
                    </span>
                    <f:display bean="food" property="foodGroup" />
                </li>
            </ol>
            <g:form resource="${this.food}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.food}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
