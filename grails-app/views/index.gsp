<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Welcome to Grails</title>
</head>
<body>

    <div class="svg" role="presentation">
        <div class="grails-logo-container">
            <asset:image src="nutreat-banner.png" class="grails-logo"/>
        </div>
    </div>

    <div id="content" role="main">
        <section class="row colset-2-its">
            <h1>Demo de Listas de Intercambio</h1>
        </section>
        <section class="row colset-3-article">
            <p style="font-size: 125%">
                El demo de listas de intercambio consiste en un draft de funcionalidad y apariencia (algo cruda) de lo que puede ser un programa más completo. En esta etapa se puede observar cómo crear grupos de alimentos, con alimentos específicos por cada grupo, y con listas de intercambio que indiquen cuántas porciones son recomendadas, así como las unidades de medida que constituyen una "porción" <i>(menú arriba a la derecha)</i>. La base de la lista de intercambio es la ingesta de kilocalorías diarias prescritas por paciente.
            </p>
            <br>
            <p style="font-size: 125%">
                Todo feedback es bienvenido, la idea es mostrar un esqueleto de un programa más sofisticado, que será desarrollado con base en retroalimentación y mayor detalle de las necesidades del negocio.
            </p>
        </section>
    </div>

</body>
</html>
