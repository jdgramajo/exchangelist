<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'foodGroup.label', default: 'FoodGroup')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-foodGroup" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="edit-foodGroup" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.foodGroup}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.foodGroup}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.foodGroup}" method="PUT">
                <g:hiddenField name="version" value="${this.foodGroup?.version}" />
                <fieldset class="form">
                    <f:field bean="foodGroup" property="name" />
                    <f:field bean="foodGroup" property="remarks" />
                    <div class='fieldcontain'>
                        <span id="name-label" class="property-label">
                            <b><g:message code="dailyServingsHeader.dailyServingsDetail.label" default="Foods" /></b>
                        </span>
                        <ul style="list-style: none;">
                            <g:each in="${foodGroup.food}" var="food">
                                <li>
                                    <span class="property-value" aria-labelledby="name-label">
                                        <g:link controller="food" action="show" id="${food.id}">${food.name}</g:link>
                                    </span>
                                </li>
                            </g:each>
                            <li>
                                <span class="property-value" aria-labelledby="name-label">
                                    <a href="/food/create?foodGroup.id=${foodGroup.id}">
                                        <g:message code="addFood.label" default="Add food" />
                                    </a>
                                </span>
                            </li>
                        </ul>
                    </div>
                </fieldset>
                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
