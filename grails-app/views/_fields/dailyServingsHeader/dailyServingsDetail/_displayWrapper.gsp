<ul style="list-style: none;">
    <g:each in="${value.sort{dsd1, dsd2 -> dsd1.foodGroup.name.compareTo(dsd2.foodGroup.name)}}" var="dailyServingsDetail">
        <li>
            <span class="property-value" aria-labelledby="${label}">
                ${dailyServingsDetail.amount} porciones de ${dailyServingsDetail.foodGroup.name}
            </span>
        </li>
    </g:each>
</ul>
