<span class="property-value" aria-labelledby="${label}">
    <g:link controller="measureUnit" action="show" id="${value.id}">${value.unit}</g:link>
</span>