<span class="property-value" aria-labelledby="${label}">
    <g:link controller="foodGroup" action="show" id="${value.id}">${value.name}</g:link>
</span>