<span class="property-value" aria-labelledby="${label}">
    <g:link controller="dailyServingsHeader" action="show" id="${value.id}">${value.kcal}</g:link>
</span>
