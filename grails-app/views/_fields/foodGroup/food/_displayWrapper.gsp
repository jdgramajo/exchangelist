<ul style="list-style: none;">
    <g:each in="${value}" var="food">
        <li>
            <span class="property-value" aria-labelledby="${label}">
                <g:link controller="food" action="show" id="${food.id}">${food.name}</g:link>
            </span>
        </li>
    </g:each>
</ul>