<%@ page import="com.nutreat.DailyServingsHeader" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'dailyServingsHeader.label', default: 'Lista de intercambio')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#edit-dailyServingsHeader" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="edit-dailyServingsHeader" class="content scaffold-edit" role="main">
            <h1><g:message code="default.edit.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.dailyServingsInfo}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.dailyServingsInfo}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form url="[action:'update', controller:'dailyServingsHeader']" resource="${this.dailyServingsInfo}" method="PUT">
                <g:hiddenField name="id" value="${this.dailyServingsInfo?.id}"/>
                <g:hiddenField name="version" value="${this.dailyServingsInfo?.version}" />
                <fieldset class="form">
                    <f:field bean="dailyServingsInfo" property="kcal" label="dailyServingsHeader.kcal.label" />
                    <f:field bean="dailyServingsInfo" property="remarks" label="dailyServingsHeader.remarks.label" />
                    <div class='fieldcontain'>
                        <span id="name-label" class="property-label">
                            <b><g:message code="dailyServingsHeader.dailyServingsDetail.label" default="Porciones" /></b>
                        </span>
                        <ul style="list-style: none;">
                            <span class="property-value">
                                <table>
                                    <g:each in="${this.dailyServingsInfo.foodGroupInfo}" var="foodGroup">
                                        <tr>
                                            <td>
                                                ${foodGroup.key}
                                            </td>
                                            <td>
                                                porciones:&nbsp;&nbsp;<g:textField name="${foodGroup.key}_servings" value="${foodGroup.value}" />
                                            </td>
                                        </tr>
                                    </g:each>
                                </table>
                            </span>
                        </ul>
                    </div>
                </fieldset>
                <fieldset class="buttons">
                    <input class="save" type="submit" value="${message(code: 'default.button.update.label', default: 'Update')}" />
                </fieldset>
            </g:form>
        </div>
    </body>
</html>
